import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'ui-kit-demo';

  form: FormGroup;

  items = [
    {
      name: 'Option 1',
    },
    {
      name: 'Option 2',
    },
    {
      name: ' Option 3',
    },
  ];

  constructor() {
    this.form = new FormGroup({
      radio: new FormControl(),
      slider: new FormControl(),
      slider2: new FormControl(),
    });

    this.form.valueChanges.subscribe((data) => {
      console.log(data);
    });
  }

  ngOnInit() {}
}
