import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NouisliderModule } from 'ng2-nouislider';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HtmlRadioComponent } from './ui-kit/html-radio/html-radio.component';
import { SliderComponent } from './ui-kit/slider/slider.component';

@NgModule({
  declarations: [AppComponent, HtmlRadioComponent, SliderComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    NouisliderModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
