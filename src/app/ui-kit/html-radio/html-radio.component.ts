import { Component, OnInit, forwardRef, Input } from '@angular/core';
import {
  FormControl,
  ControlValueAccessor,
  NG_VALUE_ACCESSOR,
  NG_VALIDATORS,
} from '@angular/forms';

@Component({
  selector: 'html-radio',
  templateUrl: 'html-radio.template.html',
  styleUrls: ['html-radio.styles.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => HtmlRadioComponent),
      multi: true,
    },
  ],
})
export class HtmlRadioComponent implements ControlValueAccessor {
  public page: string = 'html-radio';
  public activeNumber: number = 0;

  @Input()
  public set data(data: any) {
    this._data = data;
    this.setValue(null, this.data[0], 0);
  }
  public get data(): any {
    return this._data;
  }
  private _data: any;

  @Input()
  public set selectedIndex(selected: number) {
    this.setValue(null, this.data[selected], selected);
  }

  //The internal data model
  private innerValue: any = '';

  //Placeholders for the callbacks which are later providesd
  //by the Control Value Accessor
  private onTouchedCallback: () => void = () => {};

  private onChangeCallback: (_: any) => void = () => {};

  //get accessor
  get value(): any {
    return this.innerValue;
  }

  //set accessor including call the onchange callback
  //Ничего здесь не обрабатывать, иначе словишь эксепшен
  set value(v: any) {
    if (v !== this.innerValue) {
      this.innerValue = v;
      this.onChangeCallback(v);
    }
  }

  //Специальный костылёк, который помогает двигаться скрипту дальше.
  public setValue(event: any, v: any, index: number) {
    if (event) event.preventDefault();
    this.activeNumber = index;
    //this.value = v;
    this.value = this.activeNumber;
    //console.log("[", this,"]", "v", "=", v);
  }

  //Set touched on blur
  onBlur() {
    this.onTouchedCallback();
  }

  //From ControlValueAccessor interface
  writeValue(value: any) {
    if (value !== this.innerValue) {
      this.innerValue = value;
    }
  }

  //From ControlValueAccessor interface
  registerOnChange(fn: any) {
    this.onChangeCallback = fn;
  }

  //From ControlValueAccessor interface
  registerOnTouched(fn: any) {
    this.onTouchedCallback = fn;
  }
}
