import { EventEmitter, Injectable } from '@angular/core';
/* tslint:disable */

/**
 * Типы данных, актуальные для этого приложения.
 */
export namespace JQueryHolderTypes {
  /**
   * Запись об использовании jQuery-библиотеки.
   * @todo: вероятно, не получится это использовать
   */
  export interface UsedLibrary {
    // список компонент, использующих библиотеку
    components: Array<string>;
    // количество обращений к библиотеке
    usageCount: number;
    // ссылка на саму библиотеку, если
    // она поставляется классом или конструктором
    lib: any;
  }

  /**
   * Хэш-таблица используемых jQuery-библиотек.
   * @todo: вероятно, не получится это использовать
   */
  export interface UsedLibraries {
    [name: string]: UsedLibrary;
  }

  /**
   * JQuery - плагин, встраиваемый в сервис
   * JQueryHolderService.
   */
  export abstract class Plugin {
    /**
     * Список jQuery-библиотек, используемых в этом
     * плагине.
     * @type {Array}
     * @todo: вероятно, не получится это использовать.
     */
    jqueryLibs: Array<string> = [];

    /**
     * ID пагина для учета глобальных инициализаций.
     */
    /**
     * ID пагина для учета глобальных инициализаций.
     */

    public pluginId!: string;

    /**
     * Инициализирован ли компонент.
     */
    /**
     * Инициализирован ли компонент.
     */

    public isInitialized: boolean = false;

    /**
     * Инициализировано ли представление (т.е.
     * призведен вызов onDocumentReady).
     */
    /**
     * Инициализировано ли представление (т.е.
     * призведен вызов onDocumentReady).
     */

    public isViewInitialized: boolean = false;

    /**
     * Хозяин плагина (как правило, компонент).
     */
    private _owner: any;

    /**
     * Обращение к хозяину (компоненту, к которому
     * принадлежит плагин).
     *
     * Подсказка: получить имя класса компонента:
     * owner.constructor.name
     *
     * @returns {any}
     */
    public get owner(): any {
      return this._owner;
    }

    public constructor(private owner_: any) {
      this._owner = owner_;
    }

    /**
     * Привязка к событию.
     * @todo Рализовать привязку к событиям
     * @param eventName
     */
    /**
     * Привязка к событию.
     * @todo Рализовать привязку к событиям
     * @param eventName
     */

    public bind!: (eventName: string) => EventEmitter<string>;

    // методы для переопределения

    /**
     * Инициализация всех используемых библиотек.
     */
    // методы для переопределения
    /**
     * Инициализация всех используемых библиотек.
     */

    public initRequires!: ($: any, jQuery: any) => void;

    /**
     * Глобальная инициализация плагина.
     * @param $
     */
    /**
     * Глобальная инициализация плагина.
     * @param $
     */

    public initPluginGlobal!: ($: any, jQuery: any) => void;

    /**
     * Локальная инициализация плагина при добавлении.
     * @param $
     */
    /**
     * Локальная инициализация плагина при добавлении.
     * @param $
     */

    public initPluginLocal!: ($: any, jQuery: any) => void;

    /**
     * Инициализация представления (аналог onDocumentReady).
     * @param $
     */
    /**
     * Инициализация представления (аналог onDocumentReady).
     * @param $
     */

    public initPluginView!: ($: any, jQuery: any) => void;
  }

  /**
   * Список JQuery-плагинов.
   */
  export type PluginList = Array<Plugin>;
}
