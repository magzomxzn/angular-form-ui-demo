import { Component, forwardRef, Input, ElementRef } from '@angular/core';
import {
  FormControl,
  ControlValueAccessor,
  NG_VALUE_ACCESSOR,
  FormGroup,
} from '@angular/forms';

import { SliderScaleComponentPlugin } from './slider.scale.plugin';

/**
 * Данные для слайдера
 */
export interface Slider {
  title: string;
  min: number;
  max: number;
  step?: number;
  tooltips?: any;
  format?: any; // Функция для форматирования данных в noUiSlider
  ticks?: Array<number>; //Значения шкалы для разметки слайдера
  tickStep?: number; //Альтернатива ticks, для разметки шкалы слайдера через равные промежутки
}

declare var jQuery: any;

/**
 *  Компонент слайдера
    Передаваемые параметры:
    @param isTriangle Треугольный ползунок слайдера
    @param type Тип слайдера:
            'slider' (с одним ползунком)
            'rangeSlider' (с двумя ползунками)
 */

@Component({
  selector: 'slider',
  templateUrl: 'slider.template.html',
  styleUrls: ['./slider.styles.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SliderComponent),
      multi: true,
    },
  ],
})
export class SliderComponent implements ControlValueAccessor {
  @Input() type: string = '';
  @Input() useInputs: boolean = false;
  @Input() fixedTooptips: boolean = false;

  public isLoading: boolean = false;
  public _options: Slider = { title: '', min: 0, max: 100 };
  public page: string = 'slider';
  public sliderConfig: any = {};

  public model: any;

  public propsForm: any;

  @Input() set options(options: Slider) {
    this.isLoading = true;
    this._options = options;

    this.propsForm = new FormGroup({
      range: new FormControl(
        this.type == 'rangeSlider'
          ? [this._options.min, this._options.max]
          : this._options.max
      ),
      inputMin: new FormControl(
        this.type == 'rangeSlider' ? this._options.min : 0
      ),
      inputMax: new FormControl(this._options.max),
    });

    let format = {
      to: function (value: any) {
        return parseInt(value);
      },
      from: function (value: any) {
        return parseInt(value);
      },
    };

    this.model =
      this.type == 'rangeSlider'
        ? [
            this._options.format
              ? this._options.format.to(this._options.min)
              : format.to(this._options.min),
            this._options.format
              ? this._options.format.to(this._options.max)
              : format.to(this._options.max),
          ]
        : this._options.format
        ? this._options.format.to(this._options.max)
        : format.to(this._options.max);

    this.sliderConfig = {
      behaviour: 'drag-tap',
      connect: this.type == 'rangeSlider' ? true : [true, false],
      range: {
        min: this._options.min,
        max:
          this._options.max == this._options.min
            ? this._options.max + 1
            : this._options.max,
      },
      tooltips:
        this._options.tooltips !== undefined ? this._options.tooltips : true,
      step: this._options.step ? this._options.step : 1,
      format: this._options.format || format,
      /*pips: {
                mode: 'positions',
                values: [0, 100],
                density: this._options.max
            }*/
    };

    this.value = this.model;

    this.propsForm.controls['range'].valueChanges.subscribe((event: any) => {
      if (Array.isArray(event)) {
        this.propsForm.controls['inputMin'].setValue(
          this.sliderConfig.format.from(event[0]),
          {
            emitEvent: false,
          }
        );
        this.propsForm.controls['inputMax'].setValue(
          this.sliderConfig.format.from(event[1]),
          {
            emitEvent: false,
          }
        );
      } else {
        this.propsForm.controls['inputMin'].setValue(0, { emitEvent: false });
        this.propsForm.controls['inputMax'].setValue(
          parseFloat(this.sliderConfig.format.from(event)),
          { emitEvent: false }
        );
      }
      this.onChange(event);
    });

    this.propsForm.controls['inputMin'].valueChanges.subscribe((event: any) => {
      if (
        isNaN(event) ||
        this._options.min > event ||
        this._options.max < event
      )
        return;
      let value = [
        parseFloat(event),
        parseFloat(this.propsForm.controls['inputMax'].value),
      ];
      this.propsForm.controls['range'].setValue(value, { emitEvent: false });
    });

    this.propsForm.controls['inputMax'].valueChanges.subscribe((event: any) => {
      if (
        isNaN(event) ||
        this._options.min > event ||
        this._options.max < event
      )
        return;
      let value;
      if (this.type == 'rangeSlider') {
        value = [
          parseFloat(this.propsForm.controls['inputMin'].value),
          parseFloat(event),
        ];
      } else {
        value = parseFloat(event);
      }
      this.propsForm.controls['range'].setValue(value, { emitEvent: false });
    });
  }

  //Уникальное id для элемента
  public id: string;

  constructor(public element: ElementRef) {
    this.id = 'slider' + new Date().getTime();
  }

  public ngAfterViewInit(): void {
    //Инициализировать плагин, если нужна разметка шкалы слайдера
    if (
      this._options.tickStep &&
      (this._options.tickStep > 0 || this._options.ticks)
    ) {
      let self = this;
      setTimeout(() => {
        self._initPlugins();
        jQuery.initView(self);
      }, 0);
    }
  }

  private _initPlugins(): void {
    let thePlugin = new SliderScaleComponentPlugin(
      this,
      this._options.format ? this._options.format.to : null
    );
    jQuery.addPlugin(thePlugin);
  }

  //The internal data model
  private innerValue: any = '';

  //Placeholders for the callbacks which are later providesd
  //by the Control Value Accessor
  private onTouchedCallback: () => void = () => {};

  private onChangeCallback: (_: any) => void = () => {};

  onChange(event: any) {
    //console.info(event)
    this.model = event;
    /**
     * Костыль, обратное форматирование в слайдере не работает.
     */
    if (typeof event === 'string') {
      event = this.sliderConfig.format.from(event);
    }
    if (Array.isArray(event)) {
      let temp = [];
      for (let val of event) {
        if (typeof val === 'string')
          temp.push(this.sliderConfig.format.from(val));
        else temp.push(parseFloat(val)); //Предполагается что число
      }
      event = temp;
    }
    this.value = event;
  }

  //get accessor
  get value(): any {
    return this.innerValue;
  }

  //set accessor including call the onchange callback
  //Ничего здесь не обрабатывать, иначе словишь эксепшен
  set value(v: any) {
    if (v !== this.innerValue) {
      this.innerValue = v;
      this.onChangeCallback(v);
    }
  }

  //Set touched on blur
  onBlur() {
    this.onTouchedCallback();
  }

  //From ControlValueAccessor interface
  writeValue(value: any) {
    if (value !== this.innerValue) {
      this.innerValue = value;
    }
  }

  //From ControlValueAccessor interface
  registerOnChange(fn: any) {
    this.onChangeCallback = fn;
  }

  //From ControlValueAccessor interface
  registerOnTouched(fn: any) {
    this.onTouchedCallback = fn;
  }
}
