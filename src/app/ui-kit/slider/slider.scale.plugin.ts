import { JQueryHolderTypes } from './jquery-holder.types';
import { SliderComponent } from './slider.component';

export class SliderScaleComponentPlugin extends JQueryHolderTypes.Plugin {
  /**
   * Плагин для нанесение разметки на шкалу слайдера
   * TODO: Зарефакторить нативно
   */

  /**
   * ID пагина для учета глобальных инициализаций.
   */
  pluginId: string = 'SLIDER-SCALE';

  /**
   * Список jQuery-библиотек, используемых в этом
   * плагине.
   * @type {Array}
   */
  jqueryLibs: Array<string> = [
    //        'owl.carousel'
  ];

  public constructor(owner_: any, private formatFunc: (value: any) => string) {
    super(owner_);
  }

  /**
   * Инициализация представления (аналог onDocumentReady).
   * @param $
   */
  public initPluginView: ($: any, jQuery: any) => void = ($) => {
    let self = this;
    $(document).ready(function () {
      let theOwner: SliderComponent = self.owner;

      let theSlider = $('.range-slider.' + theOwner.id),
        theSepratorsWrap = theSlider.find('.separators-wrap'),
        theTicks = theOwner._options.ticks,
        theTicksStep = theOwner._options.tickStep || 10,
        theSliderMin = theOwner._options.min,
        theSliderMax = theOwner._options.max,
        theElWidth = theSepratorsWrap.width(),
        theKoef = theElWidth / theSliderMax;

      //Добавление метки шкалы в определенной позиции (в единицах слайдера)
      let addTickForPos = function (pos: number) {
        //19 - половина длины ползунка, чтобы значения были посредине
        let thePos = (pos - theSliderMin) * theKoef;

        //if (thePos>theElWidth) return;
        let value = self.formatFunc ? self.formatFunc(pos) : shortenValue(pos);
        theSepratorsWrap.append(
          '<div class="sepElem" style="left:' +
            thePos / 20 +
            'rem;"><span>' +
            value +
            '</span></div>'
        );
      };

      let shortenValue = function (value: number) {
        if (value < 1000) return value + '';
        if (value < 1000000) return value / 1000 + ' ' + 'тыс.';
        if (value < 1000000000) return value / 1000000 + ' ' + 'млн.';
        return value + '';
      };

      //Сгенерировать шкалу по предопределенным значениям
      if (theTicks) {
        for (let theTickPos of theTicks) addTickForPos(theTickPos);
      }
      //Сгенерировать шкалу через равные промежутки
      else if (0 < theSliderMax && 0 != theTicksStep) {
        let theTickOffset = theTicksStep | 0;
        while (theTickOffset < theSliderMax) {
          addTickForPos(theTickOffset);
          theTickOffset += theTicksStep;
        }
      }
    });

    // /* Добавление разделителей */
    // if($(this).hasClass('separator-slider')) {
    //     $(this).find('.range-slider-info').css({'opacity': '0'});
    //
    //     var sliderSeparatorsData = $(this).attr('data-separators').split(',');
    //     var sliderWidth = $(this).find('.separators-wrap').width();
    //     var separatorPosition = 0;
    //
    //     for(var i=0;i<=sliderSeparatorsData.length-1;i++) {
    //         console.log(separatorPosition + ' ' + i + ' ' + sliderSeparatorsData[i]);
    //         $(this).find('.separators-wrap').append('<div class="sepElem" style="left:'+separatorPosition+'px;"><span>'+sliderSeparatorsData[i]+'</span></div>');
    //         separatorPosition += sliderWidth / (sliderSeparatorsData.length-1);
    //     }
    // }
  };

  /**
   * Инициализация всех используемых библиотек.
   */
  public initRequires: ($: any, jQuery: any) => void = ($, jQuery) => {
    //require('owl.carousel');
  };
}
